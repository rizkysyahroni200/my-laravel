import 'package:flutter/material.dart';
import 'package:jababeka_report/pages/form_report.dart';
import 'package:jababeka_report/pages/home.dart';
import 'package:jababeka_report/pages/my_profile.dart';
import 'package:jababeka_report/pages/register.dart';
import 'pages/splash_screen.dart';
import 'pages/login.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<void> loadDotEnv() async {
  try {
    await dotenv.load(fileName: 'assets/.env');
    print('.env loaded successfully');
  } catch (e) {
    print('Error loading .env file: $e');
  }
}

void main() async {
  loadDotEnv().then((_) {
    runApp(
      MaterialApp(
        initialRoute: '/',
        routes: {
          '/': (context) => const SplashScreen(),
          '/login': (context) => const Login(),
          '/register': (context) => const Register(),
          '/home': (context) => const Home(),
          '/add_report': (context) => const FormReport(),
          '/my_profile': (context) => const MyProfile(),
        },
      ),
    );
  });
}
