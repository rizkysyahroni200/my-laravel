import 'dart:io';
import 'dart:convert';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:jababeka_report/services/list_reports.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const List<String> list = [
  'Accident',
  'Criminal',
  'Environment',
  'Infrastructure'
];

dynamic _convertDataToJSONCompatible(dynamic value) {
  if (value is ListReports) {
    return {
      "id": value.id,
      "message": value.message,
      "location": value.location,
      "author": value.author,
      "header": value.header,
      "category": value.category,
      "status": value.status,
      "file": value.file,
      "image": value.image,
      "createdAt": value.createdAt,
    };
  } else if (value is Map<String, dynamic>) {
    return value.map(
        (key, value) => MapEntry(key, _convertDataToJSONCompatible(value)));
  } else if (value is List) {
    return value.map((e) => _convertDataToJSONCompatible(e)).toList();
  } else {
    return value;
  }
}

class FormReport extends StatefulWidget {
  const FormReport({super.key});

  @override
  State<FormReport> createState() => _FormReportState();
}

class _FormReportState extends State<FormReport> {
  final FlutterSecureStorage _storage = FlutterSecureStorage();
  final String? url_backend = dotenv.env["BACKEND_URL"];

  String? _id;

  String header = "";
  String message = "";
  String location = "";
  String categoryValue = list.first;
  String imageName = "";
  String fileName = "";
  String messageError = "";
  String username = "";

  File? _image;
  File? _document;

  void getStoredValues(keys) async {
    String? store = await _storage.read(key: keys);

    setState(() {
      _id = store;
    });
  }

  @override
  void initState() {
    super.initState();
    getStoredValues("id");
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments;
    Map<String, dynamic>? _jsonData;

    if (args != null && args is Map<String, dynamic>) {
      final Map<String, dynamic> data = args;
      _jsonData = _convertDataToJSONCompatible(data);
      String? _jsonString;

      username = _jsonData?["author"];
    }

    if (imageName.isEmpty) {
      imageName = _jsonData?["data"]?["image"] ?? imageName;
    }

    if (fileName.isEmpty) {
      fileName = _jsonData?["data"]?["file"] ?? fileName;
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
        ),
        body: Card(
          color: Colors.white,
          child: Container(
            padding: const EdgeInsets.fromLTRB(50, 20, 50, 50),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Text(
                  "Write your problem report",
                  style: TextStyle(color: Colors.grey),
                ),
                SizedBox(
                  height: 20,
                ),
                Text("Header :"),
                Container(
                  child: TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'Fill your header',
                    ),
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    initialValue:
                        header.isEmpty ? _jsonData?["data"]?["header"] : header,
                    onChanged: (String value) {
                      setState(() {
                        header = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text("Choose category :"),
                Container(
                  width: double.infinity,
                  child: DropdownButton<String>(
                    value: categoryValue,
                    icon: null,
                    elevation: 16,
                    style: const TextStyle(color: Colors.deepPurple),
                    underline: Container(
                      height: 2,
                      color: Colors.deepPurpleAccent,
                    ),
                    onChanged: (String? value) {
                      setState(() {
                        categoryValue = value!;
                      });
                    },
                    items: list.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text("Write your report :"),
                Container(
                  child: TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'Fill your text report',
                    ),
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    initialValue:
                        message.isEmpty ? _jsonData?["data"]?["message"] : "",
                    onChanged: (String value) {
                      setState(() {
                        message = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text("Location area :"),
                Container(
                  child: TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'Fill your location',
                    ),
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    initialValue:
                        location.isEmpty ? _jsonData?["data"]?["location"] : "",
                    onChanged: (String value) {
                      setState(() {
                        location = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text("Image:"),
                Text(imageName.isEmpty ? "Image tidak ada" : imageName),
                SizedBox(
                  width: 100,
                  child: ElevatedButton(
                    onPressed: () async {
                      final result = await FilePicker.platform.pickFiles();
                      if (result == null) return null;

                      final file = result.files.first;
                      openFile(file);

                      final newFile = await saveFilePermanently(file);

                      setState(() {
                        imageName = file.name;
                        _image = File(newFile.path!);
                      });
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                    ),
                    child: const Text(
                      "Upload",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text("Attachment:"),
                Text(fileName.isEmpty ? "File tidak ada" : fileName),
                SizedBox(
                  width: 100,
                  child: ElevatedButton(
                    onPressed: () async {
                      final result = await FilePicker.platform.pickFiles();
                      if (result == null) return null;

                      final file = result.files.first;

                      openFile(file);

                      final newFile = await saveFilePermanently(file);

                      setState(() {
                        fileName = file.name;

                        _document = File(newFile.path!);
                      });
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                    ),
                    child: const Text(
                      "Upload",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 250, // Custom width for the button
                            child: ElevatedButton(
                              onPressed: () {
                                if (args != null) {
                                  editReport(
                                      (_jsonData?["data"]?["id"]).toString());
                                } else {
                                  addReport();
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.red,
                              ),
                              child: const Text(
                                "Send Report",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      messageError,
                      style: TextStyle(color: Colors.red),
                    ),
                  ],
                )
              ],
            ),
          ),
        ));
  }

  Future<File> saveFilePermanently(PlatformFile file) async {
    final appStorage = await getApplicationCacheDirectory();
    final newFile = File('${appStorage.path}/${file.name}');

    return File(file.path!).copy(newFile.path);
  }

  void openFile(PlatformFile file) {
    OpenFile.open(file.path);
  }

  void addReport() async {
    if (_image == null || _document == null) {
      print("Image or document is not selected");
      return;
    }

    var request = http.MultipartRequest(
        'POST', Uri.parse("$url_backend/web/post/create"));
    request.fields['author'] = username;
    request.fields['category'] = categoryValue;
    request.fields['header'] = header;
    request.fields['location'] = location;
    request.fields['user_id'] = _id!;
    request.fields['message'] = message;

    request.files.add(await http.MultipartFile.fromPath('image', _image!.path));
    request.files
        .add(await http.MultipartFile.fromPath('document', _document!.path));

    try {
      var response = await request.send();
      var responseConvertion = await http.Response.fromStream(response);
      print("Response body: ${responseConvertion.body}");

      if (response.statusCode == 200) {
        Navigator.pushNamed(context, '/home');
      }
      print("Response ${response}");
    } catch (error) {
      print("Error sending report: $error");
      setState(() {
        messageError = "Error sending report: $error";
      });
    }
  }

  void editReport(String id) async {
    var request = http.MultipartRequest(
        'POST', Uri.parse("$url_backend/web/post/update"));
    request.fields['author'] = username;
    request.fields['category'] = categoryValue;
    request.fields['header'] = header;
    request.fields['location'] = location;
    request.fields['user_id'] = _id!;
    request.fields['message'] = message;
    request.fields['id'] = id;

    if (_image != null) {
      request.files
          .add(await http.MultipartFile.fromPath('image', _image!.path));
    }

    if (_document != null) {
      request.files
          .add(await http.MultipartFile.fromPath('document', _document!.path));
    }

    try {
      var response = await request.send();
      var responseConvertion = await http.Response.fromStream(response);
      print("Response body: ${responseConvertion.body}");

      if (response.statusCode == 200) {
        Navigator.pushNamed(context, '/home');
      }
      print("Response ${response}");
    } catch (error) {
      print("Error sending report: $error");
      setState(() {
        messageError = "Error sending report: $error";
      });
    }
  }

}
