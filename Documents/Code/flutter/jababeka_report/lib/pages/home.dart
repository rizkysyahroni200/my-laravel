import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jababeka_report/services/list_reports.dart';
import 'package:http/http.dart' as http;

const List<String> list = [
  'All',
  'Accident',
  'Criminal',
  'Environment',
  'Infrastructure'
];

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final FlutterSecureStorage _storage = FlutterSecureStorage();
  List<dynamic> dataPosts = [];
  String searchValue = "";
  String dropdownValue = list.first;
  String? _username;
  void getStoredValues(keys) async {
    String? store = await _storage.read(key: keys);

    setState(() {
      _username = store;
    });
  }

  @override
  void initState() {
    super.initState();
    getStoredValues("username");
    fetchPostsData();
  }

  void fetchPostsData() async {
    String url_list_category =
        "http://10.0.2.2:4000/web/post/list?author=${searchValue}&header=${searchValue}&message=${searchValue}&location=${searchValue}&status=${searchValue}";
    print("dropdownValue ${dropdownValue}");
    if (dropdownValue != "All") {
      url_list_category += "?category=${dropdownValue}";
    }

    http.Response response = await http.get(
      Uri.parse(url_list_category),
    );
    Map<String, dynamic> responseData = json.decode(response.body);
    if (response.statusCode == 200) {
      print('Response body: ${response.body}');
      // dataPosts = [response.body];

      setState(() {
        dataPosts = responseData["data"];
      });
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  void onChangeSearchValue(String newValue) {
    setState(() {
      searchValue = newValue;
    });
    fetchPostsData(); // Call fetchPostsData when dropdown changes
  }

  void onDropdownChanged(String newValue) {
    setState(() {
      dropdownValue = newValue;
    });
    fetchPostsData(); // Call fetchPostsData when dropdown changes
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Hello, ${_username}",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red,
        actions: [
          ElevatedButton(
            onPressed: () async {
              Navigator.pushNamed(context, '/login');
            },
            child: Icon(
              Icons.exit_to_app,
              color: Colors.red,
            ),
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          children: <Widget>[
            SearchBar(
              onChangeSearchValue: onChangeSearchValue,
              onDropdownChanged: onDropdownChanged,
            ),
            Expanded(
              child: ListView(
                children: dataPosts.map((report) {
                  if (report['author'] == _username || _username == "admin") {
                    return ReportTemplate(
                      listReports: ListReports(
                        id: report['id'],
                        category: report['category'],
                        message: report['message'],
                        location: report['location'],
                        author: report['author'],
                        image: report['image'],
                        file: report['file'],
                        status: report['status'],
                        header: report['header'],
                        createdAt: report['createdAt'],
                      ),
                      username: _username,
                    );
                  } else {
                    return SizedBox.shrink();
                  }
                }).toList(),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                padding: EdgeInsets.only(top: 20),
                margin: EdgeInsets.only(bottom: 1, right: 1),
                child: SizedBox(
                  height: 65,
                  width: 70,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/add_report');
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                    ),
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SearchBar extends StatefulWidget {
  final Function(String) onChangeSearchValue;
  final Function(String) onDropdownChanged;
  const SearchBar(
      {Key? key,
      required this.onChangeSearchValue,
      required this.onDropdownChanged})
      : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  String dropdownValue = list.first;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(10),
              child: TextFormField(
                decoration: const InputDecoration(
                  hintText: 'Search in here',
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                onChanged: (String? value) {
                  widget.onChangeSearchValue(value!);
                },
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(0, 15, 10, 0),
            child: DropdownButton<String>(
              value: dropdownValue,
              icon: null,
              elevation: 16,
              style: const TextStyle(color: Colors.deepPurple),
              underline: Container(
                height: 2,
                color: Colors.deepPurpleAccent,
              ),
              onChanged: (String? value) {
                setState(() {
                  dropdownValue = value!;
                });
                widget.onDropdownChanged(dropdownValue);
              },
              items: list.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }
}

class ReportTemplate extends StatelessWidget {
  final ListReports listReports;
  String? username;
  ReportTemplate({required this.listReports, required this.username});
  final String? url_backend = dotenv.env["BACKEND_URL"];


  String formatDate(DateTime date) {
    List<String> indonesianMonths = [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember"
    ];

    List<String> indonesianDays = [
      "Minggu",
      "Senin",
      "Selasa",
      "Rabu",
      "Kamis",
      "Jumat",
      "Sabtu"
    ];

    return "${indonesianDays[date.weekday]} ${date.day} ${indonesianMonths[date.month]} ${date.year}";
  }

  @override
  Widget build(BuildContext context) {
    void deleteReport(num id) async {
      var request = http.MultipartRequest(
          'GET', Uri.parse("$url_backend/web/post/delete/$id"));

      try {
        var response = await request.send();
        var responseConvertion = await http.Response.fromStream(response);
        print("Response body: ${responseConvertion.body}");

        if (response.statusCode == 200) {
          Navigator.pushNamed(context, '/home');
        }
        print("Response ${response}");
      } catch (error) {
        print("Error sending report: $error");
        // setState(() {
        //   messageError = "Error sending report: $error";
        // });
      }
    }

    return Card(
      child: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            listReports.status != "Finish"
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            SizedBox(
                              child: ElevatedButton(
                                  onPressed: () async {
                                    Navigator.pushNamed(context, '/add_report',
                                        arguments: {
                                          'data': listReports,
                                          "author": username
                                        });
                                  },
                                  child: Icon(
                                    Icons.edit,
                                    color: Colors.red,
                                  )),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: SizedBox(
                          child: ElevatedButton(
                              onPressed: () async {
                                deleteReport(listReports.id);
                              },
                              child: Icon(
                                Icons.delete,
                                color: Colors.red,
                              )),
                        ),
                      )
                    ],
                  )
                : Container(),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.location_city,
                        color: Colors.red,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        listReports.location,
                        style: TextStyle(color: Colors.grey),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                ),
                Text(
                  listReports.category,
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  listReports.header,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
              width: double.infinity,
              child: Text(
                listReports.message,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Image.network(
                'http://10.0.2.2:4000/public/uploads/images/${listReports.image}'),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(8),
                  color: listReports.status == "Finish"
                      ? Colors.green
                      : Colors.red,
                  child: Text(
                    listReports.status,
                    style: TextStyle(fontSize: 15, color: Colors.white),
                  ),
                ),
                listReports.file != ""
                    ? Container(
                        child: SizedBox(
                          child: TextButton(
                            onPressed: () {
                              launch(
                                  'http://10.0.2.2:4000/public/uploads/files/${listReports.file}');
                            },
                            style: TextButton.styleFrom(
                              padding: EdgeInsets.zero, // Remove padding
                              minimumSize: Size.zero, // Allow minimum size
                              tapTargetSize: MaterialTapTargetSize
                                  .shrinkWrap, // Reduce tap target size
                            ),
                            child: const Text(
                              "Link Document",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline,
                                  decorationColor: Colors.blue),
                            ),
                          ),
                        ),
                      )
                    : Container()
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              child: Text(formatDate(DateTime.parse(listReports.createdAt))),
            ),
            SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}

class CommentTemplate extends StatelessWidget {
  const CommentTemplate({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: Row(children: <Widget>[
                Icon(Icons.person),
                Text(" Admin : Akan di proses"),
              ]),
            ),
            Container(
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.star,
                    color: Colors.orange,
                  )
                ],
              ),
            )
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          children: <Widget>[
            Icon(Icons.person),
            Text(" You : Ok min"),
          ],
        )
      ],
    );
  }
}
