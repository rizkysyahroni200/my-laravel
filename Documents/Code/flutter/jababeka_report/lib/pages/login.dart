import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class Login extends StatefulWidget {
  const Login({Key? key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final FlutterSecureStorage _storage = FlutterSecureStorage();

  String username = "";
  String password = "";
  String? messageError;
  final String? url_backend = dotenv.env["BACKEND_URL"];

  void handleLogin() async {
    final map = <String, dynamic>{};
    map['username'] = username;
    map['password'] = password;

    http.Response response = await http.post(
      Uri.parse("$url_backend/api/auth/login"),
      body: map,
    );

    Map<String, dynamic> responseBody = json.decode(response.body);
    print("response ${response.statusCode}");
    print("responseBody ${responseBody}");
    if (response.statusCode != 200) {
      return setState(() {
        messageError = responseBody['message'];
      });
    }

    final dynamic _id = responseBody['data']?['id'];
    final dynamic _username = responseBody['data']?['username'];

    if (_id != null && _username != null) {
      await _storage.write(key: 'id', value: _id.toString());
      await _storage.write(key: 'username', value: _username.toString());
    }

    Navigator.pushNamed(context, '/home');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: const Text(
          "Jababeka Report",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        leading: const SizedBox(
          width: 80.0, // adjust the width as needed
          height: 80.0, // adjust the height as needed
          child: Image(image: AssetImage('assets/logo.png')),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(50, 50, 50, 50),
        child: Column(
          crossAxisAlignment:
              CrossAxisAlignment.start, // Align children to the left
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Sign In",
                  style: TextStyle(fontSize: 30),
                ),

                Container(
                  child: Row(
                    children: <Widget>[
                      // OutlinedButton(
                      //   onPressed: () {},
                      //   style: OutlinedButton.styleFrom(
                      //     side: BorderSide(color: Colors.grey), // Border color
                      //     shape: RoundedRectangleBorder(
                      //       borderRadius:
                      //           BorderRadius.circular(100), // Rounded corners
                      //     ),
                      //   ),
                      //   child: Icon(
                      //     Icons.facebook_rounded,
                      //     color: Colors.blue,
                      //   ),
                      // ),
                      // SizedBox(width: 10),
                      // OutlinedButton(
                      //   onPressed: () {},
                      //   style: OutlinedButton.styleFrom(
                      //     side: BorderSide(color: Colors.grey), // Border color
                      //     shape: RoundedRectangleBorder(
                      //       borderRadius:
                      //           BorderRadius.circular(100), // Rounded corners
                      //     ),
                      //   ),
                      //   child: Icon(
                      //     Icons.email,
                      //     color: Colors.red,
                      //   ),
                      // ),
                    ],
                  ),
                ), // Adjust spacing between text and button
              ],
            ),
            const Text(
              "Welcome back",
              style: TextStyle(color: Colors.grey),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: TextFormField(
                decoration: const InputDecoration(
                  hintText: 'Enter your username',
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }

                  return null;
                },
                onChanged: (String value) {
                  setState(() {
                    username = value;
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: TextFormField(
                obscureText: true,
                enableSuggestions: false,
                autocorrect: false,
                decoration: const InputDecoration(
                  hintText: 'Enter your password',
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }

                  // return null;
                },
                onChanged: (String value) {
                  setState(() {
                    password = value;
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 30, 0, 0),
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 100, // Custom width for the button
                    child: ElevatedButton(
                      onPressed: () {
                        // Navigator.pushNamed(context, '/home');
                        handleLogin();
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.red,
                      ),
                      child: const Text(
                        "Sign In",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  // const SizedBox(width: 20), // Adjust spacing between buttons
                  // SizedBox(
                  //   width: 180, // Custom width for the button
                  //   child: TextButton(
                  //     onPressed: () {},
                  //     style: TextButton.styleFrom(
                  //       padding: EdgeInsets.zero, // Remove padding
                  //       minimumSize: Size.zero, // Allow minimum size
                  //       tapTargetSize: MaterialTapTargetSize
                  //           .shrinkWrap, // Reduce tap target size
                  //     ),
                  //     child: const Text(
                  //       "Forgot Password",
                  //       style: TextStyle(
                  //         color: Colors.grey,
                  //       ),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            messageError != null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        messageError!,
                        style: TextStyle(color: Colors.red),
                      )
                    ],
                  )
                : Container(),
            Expanded(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 250, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Don't have an account?",
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      width: 60, // Custom width for the button
                      child: TextButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/register');
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.zero, // Remove padding
                          minimumSize: Size.zero, // Allow minimum size
                          tapTargetSize: MaterialTapTargetSize
                              .shrinkWrap, // Reduce tap target size
                        ),
                        child: const Text(
                          "Sign Up",
                          style: TextStyle(
                              color: Colors.red, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
