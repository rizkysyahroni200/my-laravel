import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';


class Register extends StatefulWidget {
  const Register({Key? key});

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  // static const backendUrl = String.fromEnvironment('BACKEND_URL');

  String username = "";
  String email = "";
  String phone = "";
  String password = "";
  String confirmPassword = "";
  String? messageError;
  final String? url_backend = dotenv.env["BACKEND_URL"];


  void handleRegister() async {
    final map = <String, dynamic>{};
    map['username'] = username;
    map['email'] = email;
    map['phone'] = phone;
    map['password'] = password;
    map['repeat_password'] = confirmPassword;

    http.Response response = await http.post(
      Uri.parse("$url_backend/api/auth/register"),
      body: map,
    );

    if (response.statusCode != 200) {
      Map<String, dynamic> responseBody = json.decode(response.body);

      return setState(() {
        messageError = responseBody['message'];
      });
    }

    Navigator.pushNamed(context, '/login');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: const Text(
          "Jababeka Report",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        leading: const SizedBox(
          width: 80.0, // adjust the width as needed
          height: 80.0, // adjust the height as needed
          child: Image(image: AssetImage('assets/logo.png')),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(50, 50, 50, 50),
        child: Column(
          crossAxisAlignment:
              CrossAxisAlignment.start, // Align children to the left
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Sign Up",
                  style: TextStyle(fontSize: 30),
                ), // Adjust spacing between text and button
              ],
            ),
            const Text(
              "Register yourself",
              style: TextStyle(color: Colors.grey),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: TextFormField(
                decoration: const InputDecoration(
                  hintText: 'Enter your username',
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                onChanged: (String value) {
                  setState(() {
                    username = value;
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: TextFormField(
                decoration: const InputDecoration(
                  hintText: 'Enter your email',
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                onChanged: (String value) {
                  setState(() {
                    email = value;
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: TextFormField(
                decoration: const InputDecoration(
                  hintText: 'Enter your phone',
                ),
                keyboardType: TextInputType.number,
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                onChanged: (String value) {
                  setState(() {
                    phone = value;
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: TextFormField(
                obscureText: true,
                enableSuggestions: false,
                autocorrect: false,
                decoration: const InputDecoration(
                  hintText: 'Enter your password',
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                onChanged: (String value) {
                  setState(() {
                    password = value;
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
              child: TextFormField(
                obscureText: true,
                enableSuggestions: false,
                autocorrect: false,
                decoration: const InputDecoration(
                  hintText: 'Enter your confirm password',
                ),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                onChanged: (String value) {
                  setState(() {
                    confirmPassword = value;
                  });
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 30, 0, 0),
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 300, // Custom width for the button
                    child: ElevatedButton(
                      onPressed: () {
                        handleRegister();
                        // Navigator.pushNamed(context, '/login');
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.red,
                      ),
                      child: const Text(
                        "Sign Up",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            messageError != null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        messageError!,
                        style: TextStyle(color: Colors.red),
                      )
                    ],
                  )
                : Container(),
            Expanded(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Don't have an account?",
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      width: 60, // Custom width for the button
                      child: TextButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/login');
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.zero, // Remove padding
                          minimumSize: Size.zero, // Allow minimum size
                          tapTargetSize: MaterialTapTargetSize
                              .shrinkWrap, // Reduce tap target size
                        ),
                        child: const Text(
                          "Sign In",
                          style: TextStyle(
                              color: Colors.red, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
