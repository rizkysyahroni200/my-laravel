class ListComments {
  final num id;
  final String message;
  final num rating;
  final num postId;

  ListComments({
    required this.id,
    required this.message,
    required this.rating,
    required this.postId,

  });
}
