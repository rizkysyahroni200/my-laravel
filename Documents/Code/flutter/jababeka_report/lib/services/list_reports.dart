class ListReports {
  final num id;
  final String message;
  final String location;
  final String author;
  final String header;
  final String category;
  final String status;
  final String file;
  final String image;
  final String createdAt;

  ListReports({
    required this.id,
    required this.message,
    required this.location,
    required this.author,
    required this.header,
    required this.category,
    required this.status,
    required this.file,
    required this.image,
    required this.createdAt,
  });
}
